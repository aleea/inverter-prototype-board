Version 4
SymbolType BLOCK
RECTANGLE Normal -64 -104 64 104
WINDOW 0 0 -104 Bottom 2
WINDOW 3 0 104 Top 2
WINDOW 39 0 128 Top 2
SYMATTR Prefix X
SYMATTR Value IR2110
SYMATTR ModelFile Z:\home\gilou\git\inverter-prototype-board\simulations\inverter\models\IR2110.sub
SYMATTR SpiceLine T1=-40 T2=25 T3=125 V1=10 V2=15 V3=20 tonT1=90n tonT2=120n tonT3=170n tonV1=140n tonV2=120n tonV3=100n toffT1=77n toffT2=94n toffT3=130n toffV1=115n toffV2=94n toffV3=75n tonVdd1=125n tonVdd2=120n tonVdd3=115n toffVdd1=113n toffVdd2=94n toffVdd3=72n
PIN -64 -64 LEFT 8
PINATTR PinName VDD
PINATTR SpiceOrder 1
PIN -64 -32 LEFT 8
PINATTR PinName HIN
PINATTR SpiceOrder 2
PIN -64 0 LEFT 8
PINATTR PinName SD
PINATTR SpiceOrder 3
PIN -64 32 LEFT 8
PINATTR PinName LIN
PINATTR SpiceOrder 4
PIN -64 64 LEFT 8
PINATTR PinName VSS
PINATTR SpiceOrder 5
PIN 64 -80 RIGHT 8
PINATTR PinName HO
PINATTR SpiceOrder 6
PIN 64 -48 RIGHT 8
PINATTR PinName VB
PINATTR SpiceOrder 7
PIN 64 -16 RIGHT 8
PINATTR PinName VS
PINATTR SpiceOrder 8
PIN 64 16 RIGHT 8
PINATTR PinName VCC
PINATTR SpiceOrder 9
PIN 64 48 RIGHT 8
PINATTR PinName COM
PINATTR SpiceOrder 10
PIN 64 80 RIGHT 8
PINATTR PinName LO
PINATTR SpiceOrder 11
