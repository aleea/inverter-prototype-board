Version 4
SymbolType BLOCK
RECTANGLE Normal -96 -40 112 40
WINDOW 0 8 -40 Bottom 2
WINDOW 3 8 40 Top 2
WINDOW 39 8 64 Top 2
SYMATTR Prefix X
SYMATTR Value IPP65R045C7_L1
SYMATTR ModelFile Z:\home\gilou\git\inverter-prototype-board\simulations\inverter\models\IPP65R045C7.lib
SYMATTR SpiceLine dVth=0 dRdson=0
PIN 112 -16 RIGHT 8
PINATTR PinName drain
PINATTR SpiceOrder 1
PIN -96 0 LEFT 8
PINATTR PinName gate
PINATTR SpiceOrder 2
PIN 112 16 RIGHT 8
PINATTR PinName source
PINATTR SpiceOrder 3
