Version 4
SymbolType BLOCK
RECTANGLE Normal -48 -72 64 72
WINDOW 0 8 -72 Bottom 2
WINDOW 3 8 72 Top 2
WINDOW 39 8 96 Top 2
SYMATTR Prefix X
SYMATTR Value AUIRS2181S
SYMATTR ModelFile Z:\home\gilou\git\inverter-prototype-board\simulations\inverter\models\AUIRS2181S.lib
SYMATTR SpiceLine T1=-40 T2=25 T3=125 V1=10 V2=15 V3=20 toffT1=220n toffT2=220n toffT3=220n toffV1=220n toffV2=220n toffV3=220n tonT1=180n tonT2=180n tonT3=180n tonV1=180n tonV2=180n tonV3=180n VOH=1.2 VOL=0.1
PIN -48 -48 LEFT 8
PINATTR PinName VCC
PINATTR SpiceOrder 1
PIN -48 -16 LEFT 8
PINATTR PinName HIN
PINATTR SpiceOrder 2
PIN -48 16 LEFT 8
PINATTR PinName LIN
PINATTR SpiceOrder 3
PIN -48 48 LEFT 8
PINATTR PinName com
PINATTR SpiceOrder 4
PIN 64 -48 RIGHT 8
PINATTR PinName VB
PINATTR SpiceOrder 5
PIN 64 -16 RIGHT 8
PINATTR PinName HO
PINATTR SpiceOrder 6
PIN 64 16 RIGHT 8
PINATTR PinName VS
PINATTR SpiceOrder 7
PIN 64 48 RIGHT 8
PINATTR PinName LO
PINATTR SpiceOrder 8
