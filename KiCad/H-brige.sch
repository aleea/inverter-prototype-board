EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title "Inverter Prototype Board"
Date "2019-09-18"
Rev "Draft"
Comp "Atelier Libre d'Électronique Pour l'Energie Auto-produite"
Comment1 "Gilles LONGUET"
Comment2 "H-brige block"
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2500 3250 0    50   Input ~ 0
DC_high
Text HLabel 2500 4000 0    50   Input ~ 0
DC_low
Text HLabel 8250 3500 2    50   Output ~ 0
hached_low
$Comp
L Sensor_Temperature:MCP9700AT-ETT U1
U 1 1 5DF3477C
P 1850 6750
F 0 "U1" H 1521 6704 50  0000 R CNN
F 1 "MCP9700T-E/TT" H 1521 6795 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1850 6350 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21942e.pdf" H 1700 7000 50  0001 C CNN
F 4 "MCP9700T-E/TT" H 1850 6750 50  0001 C CNN "Model"
	1    1850 6750
	1    0    0    1   
$EndComp
Wire Wire Line
	2250 6750 2350 6750
Wire Wire Line
	1850 7050 1850 7275
Wire Wire Line
	1850 6450 1850 6400
$Comp
L power:VCC #PWR08
U 1 1 5DF603E5
P 1850 6400
F 0 "#PWR08" H 1850 6250 50  0001 C CNN
F 1 "VCC" H 1867 6573 50  0000 C CNN
F 2 "" H 1850 6400 50  0001 C CNN
F 3 "" H 1850 6400 50  0001 C CNN
	1    1850 6400
	1    0    0    -1  
$EndComp
Text Label 2350 6750 0    50   ~ 0
temp
$Comp
L power:GND #PWR09
U 1 1 5DF61D08
P 1850 7275
F 0 "#PWR09" H 1850 7025 50  0001 C CNN
F 1 "GND" H 1855 7102 50  0000 C CNN
F 2 "" H 1850 7275 50  0001 C CNN
F 3 "" H 1850 7275 50  0001 C CNN
	1    1850 7275
	1    0    0    -1  
$EndComp
Text Notes 2250 6000 0    50   ~ 0
MOSFET temperature sensor\nsolder only one : smd or tht
$Comp
L power:GND #PWR011
U 1 1 5DF61F99
P 3600 7300
F 0 "#PWR011" H 3600 7050 50  0001 C CNN
F 1 "GND" H 3605 7127 50  0000 C CNN
F 2 "" H 3600 7300 50  0001 C CNN
F 3 "" H 3600 7300 50  0001 C CNN
	1    3600 7300
	1    0    0    -1  
$EndComp
Text HLabel 4500 6750 2    50   Output ~ 0
temp
$Comp
L power:VCC #PWR010
U 1 1 5DF6038C
P 3600 6425
F 0 "#PWR010" H 3600 6275 50  0001 C CNN
F 1 "VCC" H 3617 6598 50  0000 C CNN
F 2 "" H 3600 6425 50  0001 C CNN
F 3 "" H 3600 6425 50  0001 C CNN
	1    3600 6425
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 6450 3600 6425
Connection ~ 3600 7250
Wire Wire Line
	3600 7250 3600 7300
Wire Wire Line
	3600 7250 3600 7050
Wire Wire Line
	4250 7250 3600 7250
Wire Wire Line
	4250 7200 4250 7250
Wire Wire Line
	4250 6750 4000 6750
Connection ~ 4250 6750
Wire Wire Line
	4250 7000 4250 6750
$Comp
L Device:C_Small C3
U 1 1 5DF46939
P 4250 7100
F 0 "C3" H 4342 7146 50  0000 L CNN
F 1 "100n" H 4342 7055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4250 7100 50  0001 C CNN
F 3 "~" H 4250 7100 50  0001 C CNN
	1    4250 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6750 4250 6750
$Comp
L Sensor_Temperature:MCP9700AT-ETT U2
U 1 1 5DF385F0
P 3600 6750
F 0 "U2" H 3270 6704 50  0000 R CNN
F 1 "MCP9700-E/TO" H 3270 6795 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92L_HandSolder" H 3600 6350 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21942e.pdf" H 3450 7000 50  0001 C CNN
	1    3600 6750
	1    0    0    1   
$EndComp
$Sheet
S 3000 3000 1250 1250
U 5E258CFD
F0 "Half bridge 1" 50
F1 "half_bridge.sch" 50
F2 "hached" O R 4250 3500 50 
F3 "high_pwm" I L 3000 3525 50 
F4 "low_pwm" I L 3000 3675 50 
F5 "Vbus_low" I L 3000 4000 50 
F6 "Vbus_high" I L 3000 3250 50 
$EndSheet
Text HLabel 4500 3500 2    50   Output ~ 0
hached_high
$Sheet
S 6750 3000 1250 1250
U 5E27E3B4
F0 "Half bridge 2" 50
F1 "half_bridge.sch" 50
F2 "hached" O R 8000 3500 50 
F3 "high_pwm" I L 6750 3525 50 
F4 "low_pwm" I L 6750 3675 50 
F5 "Vbus_low" I L 6750 4000 50 
F6 "Vbus_high" I L 6750 3250 50 
$EndSheet
Wire Wire Line
	4500 3500 4250 3500
Wire Wire Line
	3000 3250 2750 3250
Wire Wire Line
	3000 4000 2750 4000
Wire Wire Line
	2750 3250 2750 2750
Wire Wire Line
	2750 2750 6500 2750
Wire Wire Line
	6500 2750 6500 3250
Wire Wire Line
	6500 3250 6750 3250
Connection ~ 2750 3250
Wire Wire Line
	2750 3250 2500 3250
Wire Wire Line
	6750 4000 6500 4000
Wire Wire Line
	6500 4000 6500 4500
Wire Wire Line
	6500 4500 2750 4500
Wire Wire Line
	2750 4500 2750 4000
Connection ~ 2750 4000
Wire Wire Line
	2750 4000 2500 4000
Text HLabel 2750 3525 0    50   Input ~ 0
leg1_high_drv
Text HLabel 2750 3675 0    50   Input ~ 0
leg1_low_drv
Wire Wire Line
	2750 3675 3000 3675
Wire Wire Line
	3000 3525 2750 3525
Text HLabel 6500 3525 0    50   Input ~ 0
leg2_high_drv
Text HLabel 6500 3675 0    50   Input ~ 0
leg2_low_drv
Wire Wire Line
	6500 3675 6750 3675
Wire Wire Line
	6750 3525 6500 3525
Wire Wire Line
	8250 3500 8000 3500
$EndSCHEMATC
