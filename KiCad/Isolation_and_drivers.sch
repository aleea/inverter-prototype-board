EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title "Inverter Prototype Board"
Date "2019-09-18"
Rev "Draft"
Comp "Atelier Libre d'Électronique Pour l'Energie Auto-produite"
Comment1 "Gilles LONGUET"
Comment2 "Isolation and drivers block"
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5500 3675 2    50   ~ 0
leg1_high_iso
Text Label 5500 3775 2    50   ~ 0
leg1_low_iso
Text Label 5500 3875 2    50   ~ 0
leg2_high_iso
Text Label 5500 3975 2    50   ~ 0
leg2_low_iso
$Comp
L power:GND #PWR?
U 1 1 5D825C7D
P 2625 4600
AR Path="/5D825C7D" Ref="#PWR?"  Part="1" 
AR Path="/5D824D19/5D825C7D" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 2625 4350 50  0001 C CNN
F 1 "GND" H 2630 4427 50  0000 C CNN
F 2 "" H 2625 4600 50  0001 C CNN
F 3 "" H 2625 4600 50  0001 C CNN
	1    2625 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR?
U 1 1 5D825C85
P 4375 4600
AR Path="/5D825C85" Ref="#PWR?"  Part="1" 
AR Path="/5D824D19/5D825C85" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 4375 4400 50  0001 C CNN
F 1 "GNDPWR" H 4379 4446 50  0000 C CNN
F 2 "" H 4375 4550 50  0001 C CNN
F 3 "" H 4375 4550 50  0001 C CNN
	1    4375 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3675 2900 3675
Wire Wire Line
	1800 3775 2900 3775
Wire Wire Line
	1800 3875 2900 3875
Wire Wire Line
	1800 3975 2900 3975
$Comp
L IPB-rescue:IRS2181-Driver_FET U?
U 1 1 5D8265DB
P 7000 4750
AR Path="/5D8265DB" Ref="U?"  Part="1" 
AR Path="/5D824D19/5D8265DB" Ref="U6"  Part="1" 
F 0 "U6" H 7000 5317 50  0000 C CNN
F 1 "IRS2181" H 7000 5226 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7000 4300 50  0001 C CIN
F 3 "https://www.infineon.com/dgdl/irs2181.pdf?fileId=5546d462533600a401535676c12b27d3" H 6800 4200 50  0001 C CNN
	1    7000 4750
	1    0    0    -1  
$EndComp
Text HLabel 1800 3675 0    50   Input ~ 0
leg1_high
Text HLabel 1800 3775 0    50   Input ~ 0
leg1_low
$Comp
L Device:C_Small C?
U 1 1 5D8328D7
P 6525 4625
AR Path="/5D802728/5D8328D7" Ref="C?"  Part="1" 
AR Path="/5D824D19/5D8328D7" Ref="C30"  Part="1" 
F 0 "C30" H 6433 4579 50  0000 R CNN
F 1 "100n" H 6433 4670 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6525 4625 50  0001 C CNN
F 3 "~" H 6525 4625 50  0001 C CNN
	1    6525 4625
	1    0    0    1   
$EndComp
$Comp
L Device:D_Small D?
U 1 1 5D8328FA
P 7000 4025
AR Path="/5D802728/5D8328FA" Ref="D?"  Part="1" 
AR Path="/5D824D19/5D8328FA" Ref="D13"  Part="1" 
F 0 "D13" H 7200 3950 50  0000 R CNN
F 1 "ES1J" H 6950 3950 50  0000 R CNN
F 2 "Diode_SMD:D_SMA" V 7000 4025 50  0001 C CNN
F 3 "~" V 7000 4025 50  0001 C CNN
	1    7000 4025
	-1   0    0    1   
$EndComp
Text HLabel 8300 2850 2    50   Output ~ 0
leg1_high_drv
Text HLabel 8300 3050 2    50   Output ~ 0
leg1_low_drv
Text HLabel 1800 3875 0    50   Input ~ 0
leg2_high
Text HLabel 1800 3975 0    50   Input ~ 0
leg2_low
Wire Wire Line
	6700 4450 6525 4450
Wire Wire Line
	6525 4450 6525 4525
Wire Wire Line
	6525 4725 6525 5050
Wire Wire Line
	6525 5050 6700 5050
Wire Wire Line
	7450 4525 7450 4450
Wire Wire Line
	7450 4450 7300 4450
Wire Wire Line
	7450 4725 7450 4950
Wire Wire Line
	7450 4950 7300 4950
Wire Wire Line
	6900 4025 6525 4025
Wire Wire Line
	6525 4025 6525 4450
Connection ~ 6525 4450
Wire Wire Line
	7100 4025 7450 4025
Wire Wire Line
	7450 4025 7450 4450
Connection ~ 7450 4450
Wire Wire Line
	6525 5050 6525 5225
Connection ~ 6525 5050
Wire Wire Line
	6525 4025 6525 4000
Connection ~ 6525 4025
$Comp
L power:GNDPWR #PWR040
U 1 1 5D8442EA
P 6525 5225
F 0 "#PWR040" H 6525 5025 50  0001 C CNN
F 1 "GNDPWR" H 6529 5071 50  0000 C CNN
F 2 "" H 6525 5175 50  0001 C CNN
F 3 "" H 6525 5175 50  0001 C CNN
	1    6525 5225
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4850 5925 4850
Wire Wire Line
	5925 4850 5925 3875
Wire Wire Line
	4100 3875 5925 3875
Wire Wire Line
	5825 3975 5825 4950
Wire Wire Line
	5825 4950 6700 4950
Wire Wire Line
	4100 3975 5825 3975
$Comp
L IPB-rescue:IRS2181-Driver_FET U?
U 1 1 5D84C8E5
P 7000 2750
AR Path="/5D84C8E5" Ref="U?"  Part="1" 
AR Path="/5D824D19/5D84C8E5" Ref="U5"  Part="1" 
F 0 "U5" H 7000 3317 50  0000 C CNN
F 1 "IRS2181" H 7000 3226 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7000 2300 50  0001 C CIN
F 3 "https://www.infineon.com/dgdl/irs2181.pdf?fileId=5546d462533600a401535676c12b27d3" H 6800 2200 50  0001 C CNN
	1    7000 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5D84C8EB
P 6525 2625
AR Path="/5D802728/5D84C8EB" Ref="C?"  Part="1" 
AR Path="/5D824D19/5D84C8EB" Ref="C29"  Part="1" 
F 0 "C29" H 6433 2579 50  0000 R CNN
F 1 "100n" H 6433 2670 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6525 2625 50  0001 C CNN
F 3 "~" H 6525 2625 50  0001 C CNN
	1    6525 2625
	1    0    0    1   
$EndComp
Wire Wire Line
	6700 2450 6525 2450
Wire Wire Line
	6525 2450 6525 2525
Wire Wire Line
	6525 2725 6525 3050
Wire Wire Line
	6525 3050 6700 3050
Wire Wire Line
	7450 2525 7450 2450
Wire Wire Line
	7450 2450 7300 2450
Wire Wire Line
	7450 2725 7450 2950
Wire Wire Line
	7450 2950 7300 2950
Wire Wire Line
	6850 2025 6525 2025
Wire Wire Line
	6525 2025 6525 2450
Connection ~ 6525 2450
Wire Wire Line
	7150 2025 7450 2025
Wire Wire Line
	7450 2025 7450 2450
Connection ~ 7450 2450
Wire Wire Line
	6525 3050 6525 3225
Connection ~ 6525 3050
Wire Wire Line
	6525 2025 6525 2000
Connection ~ 6525 2025
$Comp
L power:GNDPWR #PWR038
U 1 1 5D84C90F
P 6525 3225
F 0 "#PWR038" H 6525 3025 50  0001 C CNN
F 1 "GNDPWR" H 6529 3071 50  0000 C CNN
F 2 "" H 6525 3175 50  0001 C CNN
F 3 "" H 6525 3175 50  0001 C CNN
	1    6525 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3775 5925 3775
Wire Wire Line
	4100 3675 5825 3675
Text HLabel 8300 4850 2    50   Output ~ 0
leg2_high_drv
Text HLabel 8300 5050 2    50   Output ~ 0
leg2_low_drv
Wire Wire Line
	7300 4850 8300 4850
Wire Wire Line
	7300 5050 8300 5050
Wire Wire Line
	7300 2850 8300 2850
Wire Wire Line
	7300 3050 8300 3050
$Comp
L Device:C_Small C?
U 1 1 5D85FD09
P 4375 4150
AR Path="/5D802728/5D85FD09" Ref="C?"  Part="1" 
AR Path="/5D824D19/5D85FD09" Ref="C26"  Part="1" 
F 0 "C26" H 4283 4104 50  0000 R CNN
F 1 "100n" H 4283 4195 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4375 4150 50  0001 C CNN
F 3 "~" H 4375 4150 50  0001 C CNN
	1    4375 4150
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5D8787CA
P 2625 4275
AR Path="/5D802728/5D8787CA" Ref="C?"  Part="1" 
AR Path="/5D824D19/5D8787CA" Ref="C25"  Part="1" 
F 0 "C25" H 2533 4229 50  0000 R CNN
F 1 "100n" H 2533 4320 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2625 4275 50  0001 C CNN
F 3 "~" H 2625 4275 50  0001 C CNN
	1    2625 4275
	1    0    0    -1  
$EndComp
Wire Wire Line
	2625 4175 2625 3475
Wire Wire Line
	2625 3475 2900 3475
Connection ~ 2625 3475
Wire Wire Line
	2625 3250 2625 3475
$Comp
L power:VCC #PWR033
U 1 1 5D887930
P 2625 3250
F 0 "#PWR033" H 2625 3100 50  0001 C CNN
F 1 "VCC" H 2642 3423 50  0000 C CNN
F 2 "" H 2625 3250 50  0001 C CNN
F 3 "" H 2625 3250 50  0001 C CNN
	1    2625 3250
	1    0    0    -1  
$EndComp
$Comp
L IPB-cache:15V_iso #PWR037
U 1 1 5D853800
P 6525 2000
F 0 "#PWR037" H 6525 1850 50  0001 C CNN
F 1 "15V_iso" H 6540 2173 50  0000 C CNN
F 2 "" H 6525 2000 50  0001 C CNN
F 3 "" H 6525 2000 50  0001 C CNN
	1    6525 2000
	1    0    0    -1  
$EndComp
$Comp
L IPB-cache:15V_iso #PWR039
U 1 1 5D85404D
P 6525 4000
F 0 "#PWR039" H 6525 3850 50  0001 C CNN
F 1 "15V_iso" H 6540 4173 50  0000 C CNN
F 2 "" H 6525 4000 50  0001 C CNN
F 3 "" H 6525 4000 50  0001 C CNN
	1    6525 4000
	1    0    0    -1  
$EndComp
$Comp
L Isolator:ADuM1410 U4
U 1 1 5D855868
P 3500 3975
F 0 "U4" H 3050 4625 50  0000 C CNN
F 1 "ADuM140D0" H 3775 4625 50  0000 C CNN
F 2 "Package_SO:SOIC-16W_7.5x10.3mm_P1.27mm" H 3500 3275 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADuM140D_140E_141D_141E_142D_142E.pdf" H 2700 3975 50  0001 C CNN
	1    3500 3975
	1    0    0    -1  
$EndComp
Wire Wire Line
	4375 3250 4375 3475
Wire Wire Line
	4100 3475 4375 3475
Connection ~ 4375 3475
Wire Wire Line
	4375 3475 4375 4050
Wire Wire Line
	4375 4250 4375 4375
Wire Wire Line
	4100 4375 4375 4375
Connection ~ 4375 4375
Wire Wire Line
	4375 4375 4375 4475
Wire Wire Line
	4100 4475 4375 4475
Connection ~ 4375 4475
Wire Wire Line
	4375 4475 4375 4600
NoConn ~ 4100 4175
Wire Wire Line
	2625 4375 2625 4475
Wire Wire Line
	2900 4375 2625 4375
Connection ~ 2625 4375
Wire Wire Line
	2900 4475 2625 4475
Connection ~ 2625 4475
Wire Wire Line
	2625 4475 2625 4600
Wire Wire Line
	2900 4175 2875 4175
Wire Wire Line
	2875 4175 2875 4125
Wire Wire Line
	2875 4125 1800 4125
Text HLabel 1800 4125 0    50   Input ~ 0
gates_disable
Wire Wire Line
	5825 2850 6700 2850
Wire Wire Line
	5925 2950 6700 2950
Wire Wire Line
	5925 3775 5925 2950
Wire Wire Line
	5825 3675 5825 2850
Wire Wire Line
	7450 2950 8300 2950
Connection ~ 7450 2950
Text HLabel 8300 2950 2    50   Input ~ 0
leg1_vs_drv
Wire Wire Line
	7450 4950 8300 4950
Text HLabel 8300 4950 2    50   Input ~ 0
leg2_vs_drv
$Comp
L Device:D_Schottky D12
U 1 1 5D8A61CE
P 7000 2025
F 0 "D12" V 6954 2104 50  0000 L CNN
F 1 "ES1J" V 7045 2104 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 7000 2025 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1386/0900766b81386437.pdf" H 7000 2025 50  0001 C CNN
	1    7000 2025
	-1   0    0    -1  
$EndComp
Connection ~ 7450 4950
$Comp
L Device:C_Small C?
U 1 1 5DB38739
P 7450 2625
AR Path="/5D802728/5DB38739" Ref="C?"  Part="1" 
AR Path="/5D824D19/5DB38739" Ref="C31"  Part="1" 
F 0 "C31" H 7358 2579 50  0000 R CNN
F 1 "100n" H 7358 2670 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7450 2625 50  0001 C CNN
F 3 "~" H 7450 2625 50  0001 C CNN
	1    7450 2625
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DB3CD8F
P 7450 4625
AR Path="/5D802728/5DB3CD8F" Ref="C?"  Part="1" 
AR Path="/5D824D19/5DB3CD8F" Ref="C32"  Part="1" 
F 0 "C32" H 7358 4579 50  0000 R CNN
F 1 "100n" H 7358 4670 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7450 4625 50  0001 C CNN
F 3 "~" H 7450 4625 50  0001 C CNN
	1    7450 4625
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DEB0998
P 6125 2625
AR Path="/5D802728/5DEB0998" Ref="C?"  Part="1" 
AR Path="/5D824D19/5DEB0998" Ref="C27"  Part="1" 
F 0 "C27" H 6033 2579 50  0000 R CNN
F 1 "1u" H 6033 2670 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6125 2625 50  0001 C CNN
F 3 "~" H 6125 2625 50  0001 C CNN
	1    6125 2625
	1    0    0    1   
$EndComp
Wire Wire Line
	6125 2725 6125 3050
Wire Wire Line
	6125 3050 6525 3050
Wire Wire Line
	6525 2450 6125 2450
Wire Wire Line
	6125 2450 6125 2525
$Comp
L Device:C_Small C?
U 1 1 5DEB3F24
P 6150 4625
AR Path="/5D802728/5DEB3F24" Ref="C?"  Part="1" 
AR Path="/5D824D19/5DEB3F24" Ref="C28"  Part="1" 
F 0 "C28" H 6058 4579 50  0000 R CNN
F 1 "1u" H 6058 4670 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6150 4625 50  0001 C CNN
F 3 "~" H 6150 4625 50  0001 C CNN
	1    6150 4625
	1    0    0    1   
$EndComp
Wire Wire Line
	6150 4525 6150 4450
Wire Wire Line
	6150 4450 6525 4450
Wire Wire Line
	6150 4725 6150 5050
Wire Wire Line
	6150 5050 6525 5050
$Comp
L IPB-cache:5V_iso #PWR035
U 1 1 5DE6547F
P 4375 3250
F 0 "#PWR035" H 4375 3100 50  0001 C CNN
F 1 "5V_iso" H 4392 3423 50  0000 C CNN
F 2 "" H 4375 3250 50  0001 C CNN
F 3 "" H 4375 3250 50  0001 C CNN
	1    4375 3250
	1    0    0    -1  
$EndComp
Wire Notes Line
	1250 5750 8750 5750
Wire Notes Line
	1250 5925 8750 5925
Wire Notes Line
	3500 4750 3500 5925
Wire Notes Line
	7000 5250 7000 5925
Text Notes 2000 5875 0    50   ~ 0
3V3 Logic signals
Text Notes 4850 5875 0    50   ~ 0
5V Logic signals
Text Notes 7350 5875 0    50   ~ 0
floating 15V gate drivers signals
$EndSCHEMATC
