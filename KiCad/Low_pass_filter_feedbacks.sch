EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 7
Title "Inverter Prototype Board"
Date "2019-09-18"
Rev "Draft"
Comp "Atelier Libre d'Électronique Pour l'Energie Auto-produite"
Comment1 "Gilles LONGUET"
Comment2 "Low pass filter and feedbacks block"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L IPB-rescue:L_Core_Ferrite_Coupled-Device-IPB-rescue-IPB-rescue-IPB-rescue L1
U 1 1 5D8348E3
P 6150 2375
F 0 "L1" H 6150 2550 50  0000 C CNN
F 1 "2m" H 6150 2225 50  0000 C CNN
F 2 "Inductor_THT:L_CommonMode_Wuerth_WE-CMB-XL" H 6150 2375 50  0001 C CNN
F 3 "https://docs.rs-online.com/cc2d/0900766b8157af9d.pdf" H 6150 2375 50  0001 C CNN
F 4 "744830010185" H 6150 2375 50  0001 C CNN "Model"
	1    6150 2375
	1    0    0    -1  
$EndComp
$Comp
L IPB-rescue:C-Device-IPB-rescue-IPB-rescue-IPB-rescue C14
U 1 1 5D834912
P 8000 2350
F 0 "C14" H 8115 2396 50  0000 L CNN
F 1 "100n" H 8125 2300 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L18.0mm_W11.0mm_P15.00mm_FKS3_FKP3" H 8038 2200 50  0001 C CNN
F 3 "https://docs.rs-online.com/4e4d/0900766b8171c98d.pdf" H 8000 2350 50  0001 C CNN
F 4 "R463I347040M1M" H 8000 2350 50  0001 C CNN "Model"
	1    8000 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2275 6575 2275
Wire Wire Line
	6575 2275 6575 2000
Wire Wire Line
	6350 2475 6575 2475
Wire Wire Line
	6575 2475 6575 2625
Wire Wire Line
	6750 2200 6750 2000
Text Notes 5825 1725 0    118  ~ 0
filtrage LC
Text HLabel 9750 2000 2    50   Output ~ 0
Vout_high
Text HLabel 9750 2625 2    50   Output ~ 0
Vout_low
Text HLabel 2000 2000 0    50   Input ~ 0
hached_high
Text HLabel 5500 2475 0    50   Input ~ 0
hached_low
Text HLabel 9675 3900 2    50   Output ~ 0
u_feedback
Wire Wire Line
	6750 2500 6750 2625
$Comp
L IPB-rescue:C-Device-IPB-rescue-IPB-rescue-IPB-rescue C13
U 1 1 5DDFF63A
P 6750 2350
F 0 "C13" H 6865 2396 50  0000 L CNN
F 1 "10u" H 6865 2305 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L41.5mm_W31.0mm_P37.50mm_MKS4" H 6788 2200 50  0001 C CNN
F 3 "https://docs.rs-online.com/59da/0900766b8171c9dd.pdf" H 6750 2350 50  0001 C CNN
F 4 "R463W510050M1K" H 6750 2350 50  0001 C CNN "Model"
	1    6750 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R25
U 1 1 5D8816CE
P 2275 6175
F 0 "R25" V 2068 6175 50  0000 C CNN
F 1 "10k" V 2159 6175 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2205 6175 50  0001 C CNN
F 3 "~" H 2275 6175 50  0001 C CNN
F 4 "R" H 2275 6175 50  0001 C CNN "Spice_Primitive"
F 5 "100k" H 2275 6175 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2275 6175 50  0001 C CNN "Spice_Netlist_Enabled"
	1    2275 6175
	-1   0    0    1   
$EndComp
Text HLabel 4650 2875 2    50   Output ~ 0
i_feedback
Wire Wire Line
	8625 3450 8625 2000
Wire Wire Line
	8700 3850 8575 3850
$Comp
L IPB-rescue:TRANSF1-Transformer-IPB-rescue-IPB-rescue-IPB-rescue T1
U 1 1 5D83492C
P 8900 3650
F 0 "T1" H 8900 3272 50  0000 C CNN
F 1 "1213819" H 8900 3363 50  0000 C CNN
F 2 "Transformer_THT:Transformer_Breve_TEZ-22x24" H 8900 3650 50  0001 C CNN
F 3 "https://docs.rs-online.com/f485/0900766b815c22e1.pdf" H 8900 3650 50  0001 C CNN
F 4 "121-3819" H 8900 3650 50  0001 C CNN "Model"
	1    8900 3650
	1    0    0    1   
$EndComp
Wire Wire Line
	8700 3450 8625 3450
Wire Wire Line
	5500 2275 5950 2275
Wire Notes Line
	5750 1500 5750 2800
Wire Notes Line
	5750 2800 8400 2800
Wire Notes Line
	8400 2800 8400 1500
Wire Notes Line
	5750 1500 8400 1500
Wire Notes Line
	3275 7025 3275 5750
Wire Notes Line
	3275 5750 2000 5750
Text Notes 2625 5825 0    50   ~ 0
Measure offset
Text Notes 9325 3300 0    50   ~ 0
AC feedback transformer
$Comp
L Device:R R29
U 1 1 5DB459F0
P 9200 4100
F 0 "R29" H 9270 4146 50  0000 L CNN
F 1 "R" H 9270 4055 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9130 4100 50  0001 C CNN
F 3 "~" H 9200 4100 50  0001 C CNN
	1    9200 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 3450 9200 3450
Wire Wire Line
	9200 3450 9200 3550
Wire Wire Line
	9675 3900 9525 3900
Wire Wire Line
	9200 3850 9200 3900
Connection ~ 9200 3900
Wire Wire Line
	9200 3900 9200 3950
$Comp
L Device:R R28
U 1 1 5DB454A1
P 9200 3700
F 0 "R28" H 9270 3746 50  0000 L CNN
F 1 "R" H 9270 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9130 3700 50  0001 C CNN
F 3 "~" H 9200 3700 50  0001 C CNN
	1    9200 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 2200 8000 2000
Wire Wire Line
	2275 5975 2275 6025
Wire Wire Line
	2650 6400 2275 6400
Connection ~ 2275 6400
Wire Wire Line
	2275 6400 2275 6425
Wire Wire Line
	2650 6400 2650 6450
Wire Wire Line
	2650 6400 2750 6400
Connection ~ 2650 6400
Wire Notes Line
	3275 7025 2000 7025
Text Label 9025 4325 2    50   ~ 0
offset
$Comp
L Device:C C15
U 1 1 5DE9771D
P 9525 4100
F 0 "C15" H 9640 4146 50  0000 L CNN
F 1 "C" H 9640 4055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9563 3950 50  0001 C CNN
F 3 "~" H 9525 4100 50  0001 C CNN
	1    9525 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9525 3950 9525 3900
Connection ~ 9525 3900
Wire Wire Line
	9525 3900 9200 3900
Wire Wire Line
	9525 4325 9525 4250
Wire Wire Line
	9025 4325 9100 4325
Wire Wire Line
	9200 4250 9200 4325
Connection ~ 9200 4325
Wire Wire Line
	9200 4325 9525 4325
Connection ~ 9100 4325
Wire Wire Line
	9100 4325 9200 4325
Wire Wire Line
	9100 3850 9100 4325
Wire Notes Line
	8475 3175 10325 3175
Wire Notes Line
	10325 3175 10325 4600
Wire Notes Line
	10325 4600 8475 4600
Wire Notes Line
	8475 4600 8475 3175
Wire Wire Line
	5950 2475 5500 2475
$Comp
L power:VCC #PWR014
U 1 1 5DF1E404
P 2275 5975
F 0 "#PWR014" H 2275 5825 50  0001 C CNN
F 1 "VCC" H 2292 6148 50  0000 C CNN
F 2 "" H 2275 5975 50  0001 C CNN
F 3 "" H 2275 5975 50  0001 C CNN
	1    2275 5975
	1    0    0    -1  
$EndComp
Wire Wire Line
	2275 6325 2275 6400
Connection ~ 2275 6800
Wire Wire Line
	2275 6725 2275 6800
Wire Wire Line
	2650 6800 2275 6800
Wire Wire Line
	2650 6750 2650 6800
$Comp
L Device:C C12
U 1 1 5DC34432
P 2275 6575
F 0 "C12" H 2390 6621 50  0000 L CNN
F 1 "1u" H 2390 6530 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2313 6425 50  0001 C CNN
F 3 "~" H 2275 6575 50  0001 C CNN
	1    2275 6575
	1    0    0    -1  
$EndComp
Wire Notes Line
	2000 5750 2000 7025
$Comp
L Device:R R26
U 1 1 5D881EB6
P 2650 6600
F 0 "R26" V 2443 6600 50  0000 C CNN
F 1 "10k" V 2534 6600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2580 6600 50  0001 C CNN
F 3 "~" H 2650 6600 50  0001 C CNN
F 4 "R" H 2650 6600 50  0001 C CNN "Spice_Primitive"
F 5 "100k" H 2650 6600 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2650 6600 50  0001 C CNN "Spice_Netlist_Enabled"
	1    2650 6600
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR016
U 1 1 5DF1A9B0
P 4625 5900
F 0 "#PWR016" H 4625 5750 50  0001 C CNN
F 1 "VCC" H 4642 6073 50  0000 C CNN
F 2 "" H 4625 5900 50  0001 C CNN
F 3 "" H 4625 5900 50  0001 C CNN
	1    4625 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R27
U 1 1 5DF1B221
P 4625 6100
F 0 "R27" H 4695 6146 50  0000 L CNN
F 1 "R" H 4695 6055 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4555 6100 50  0001 C CNN
F 3 "~" H 4625 6100 50  0001 C CNN
	1    4625 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D9
U 1 1 5DF1B9E7
P 4625 6600
F 0 "D9" V 4579 6679 50  0000 L CNN
F 1 "D_Zener" V 4670 6679 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 4625 6600 50  0001 C CNN
F 3 "~" H 4625 6600 50  0001 C CNN
	1    4625 6600
	0    1    1    0   
$EndComp
$Comp
L IPB-rescue:GND-power-IPB-rescue-IPB-rescue-IPB-rescue #PWR015
U 1 1 5D834922
P 2275 6800
F 0 "#PWR015" H 2275 6550 50  0001 C CNN
F 1 "GND" H 2280 6627 50  0000 C CNN
F 2 "" H 2275 6800 50  0001 C CNN
F 3 "" H 2275 6800 50  0001 C CNN
	1    2275 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5DF1C551
P 4625 6925
F 0 "#PWR017" H 4625 6675 50  0001 C CNN
F 1 "GND" H 4630 6752 50  0000 C CNN
F 2 "" H 4625 6925 50  0001 C CNN
F 3 "" H 4625 6925 50  0001 C CNN
	1    4625 6925
	1    0    0    -1  
$EndComp
Wire Wire Line
	4625 5900 4625 5950
Wire Wire Line
	4625 6750 4625 6925
Wire Wire Line
	4625 6450 4625 6350
Text HLabel 5350 6350 2    50   Output ~ 0
U_ref
Wire Wire Line
	5350 6350 4625 6350
Connection ~ 4625 6350
Wire Wire Line
	4625 6350 4625 6250
Wire Notes Line
	4025 5675 5725 5675
Wire Notes Line
	5725 5675 5725 7175
Wire Notes Line
	5725 7175 4025 7175
Wire Notes Line
	4025 7175 4025 5675
Text Notes 5075 5800 0    50   ~ 0
offset reference\n
$Comp
L Transformer:CST2010 TI1
U 1 1 5DF332FC
P 3550 2475
F 0 "TI1" H 3250 2650 50  0000 C CNN
F 1 "B82801C2245A200" H 2950 2575 50  0000 C CNN
F 2 "Transformer_SMD:Transformer_Coilcraft_CST2010" H 3550 2475 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/400/b82801c-1527913.pdf" H 3550 2475 50  0001 C CNN
F 4 "B82801C2245A200" H 3550 2475 50  0001 C CNN "Model"
	1    3550 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	3125 2875 3050 2875
Connection ~ 3125 2875
Wire Wire Line
	3125 2975 3125 2875
Wire Wire Line
	3100 2975 3125 2975
Wire Wire Line
	2775 2875 2575 2875
Connection ~ 2775 2875
Wire Wire Line
	2775 2975 2775 2875
Wire Wire Line
	2800 2975 2775 2975
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 5DEA81FC
P 2950 2975
F 0 "JP1" H 2825 2875 50  0000 C CNN
F 1 "AC/DC" H 2875 2800 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 2950 2975 50  0001 C CNN
F 3 "~" H 2950 2975 50  0001 C CNN
	1    2950 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2000 3350 2000
Connection ~ 2575 3375
Connection ~ 3325 3375
Wire Wire Line
	3325 3375 2575 3375
Text Label 3000 3375 2    50   ~ 0
offset
Wire Wire Line
	3325 2875 3125 2875
Connection ~ 3325 2875
Wire Wire Line
	3325 2950 3325 2875
Wire Wire Line
	3325 3250 3325 3375
Wire Wire Line
	4075 3375 3325 3375
Wire Wire Line
	4075 3250 4075 3375
Wire Wire Line
	4075 2875 4650 2875
Connection ~ 4075 2875
Wire Wire Line
	4075 2950 4075 2875
Wire Wire Line
	3875 2875 4075 2875
Wire Wire Line
	3575 2875 3325 2875
Connection ~ 2575 2875
Wire Wire Line
	2850 2875 2775 2875
Wire Wire Line
	2575 3375 2575 3175
Wire Wire Line
	2325 3375 2575 3375
Wire Wire Line
	2325 2475 2325 3375
Wire Wire Line
	3250 2475 2325 2475
Wire Wire Line
	2575 2575 2575 2875
Wire Wire Line
	3850 2575 2575 2575
Wire Wire Line
	3850 2475 3850 2575
$Comp
L IPB-rescue:C-Device-IPB-rescue-IPB-rescue-IPB-rescue C17
U 1 1 5DE6DE78
P 4075 3100
F 0 "C17" H 4190 3146 50  0000 L CNN
F 1 "C" H 4190 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4113 2950 50  0001 C CNN
F 3 "~" H 4075 3100 50  0001 C CNN
	1    4075 3100
	-1   0    0    -1  
$EndComp
Text Notes 2400 4050 0    50   ~ 0
AC or DC current transformer
Wire Notes Line
	2275 1900 4500 1900
Wire Notes Line
	2275 4125 2275 1900
Wire Notes Line
	4500 4125 2275 4125
Wire Notes Line
	4500 1900 4500 4125
$Comp
L Device:D_Small D10
U 1 1 5DB3B283
P 2950 2875
F 0 "D10" H 2950 3080 50  0000 C CNN
F 1 "D" H 2950 2989 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" V 2950 2875 50  0001 C CNN
F 3 "~" V 2950 2875 50  0001 C CNN
	1    2950 2875
	-1   0    0    -1  
$EndComp
$Comp
L IPB-rescue:C-Device-IPB-rescue-IPB-rescue-IPB-rescue C16
U 1 1 5D8348FC
P 3325 3100
F 0 "C16" H 3440 3146 50  0000 L CNN
F 1 "C" H 3440 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3363 2950 50  0001 C CNN
F 3 "~" H 3325 3100 50  0001 C CNN
	1    3325 3100
	-1   0    0    -1  
$EndComp
$Comp
L IPB-rescue:R-Device-IPB-rescue-IPB-rescue-IPB-rescue R31
U 1 1 5D8348F0
P 3725 2875
F 0 "R31" V 3518 2875 50  0000 C CNN
F 1 "R" V 3609 2875 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3655 2875 50  0001 C CNN
F 3 "~" H 3725 2875 50  0001 C CNN
	1    3725 2875
	0    -1   1    0   
$EndComp
$Comp
L IPB-rescue:R-Device-IPB-rescue-IPB-rescue-IPB-rescue R30
U 1 1 5D8348EA
P 2575 3025
F 0 "R30" H 2645 3071 50  0000 L CNN
F 1 "R" H 2645 2980 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2505 3025 50  0001 C CNN
F 3 "~" H 2575 3025 50  0001 C CNN
	1    2575 3025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3750 2275 3750 2000
Wire Wire Line
	3350 2275 3350 2000
Wire Wire Line
	5500 2000 5500 2275
Wire Wire Line
	3750 2000 5500 2000
$Comp
L IPB-rescue:L_Core_Ferrite_Coupled-Device-IPB-rescue-IPB-rescue-IPB-rescue L2
U 1 1 5E3B6668
P 7450 2375
F 0 "L2" H 7450 2550 50  0000 C CNN
F 1 "2m" H 7450 2225 50  0000 C CNN
F 2 "Inductor_THT:L_CommonMode_Wuerth_WE-CMB-XL" H 7450 2375 50  0001 C CNN
F 3 "https://docs.rs-online.com/cc2d/0900766b8157af9d.pdf" H 7450 2375 50  0001 C CNN
F 4 "744830010185" H 7450 2375 50  0001 C CNN "Model"
	1    7450 2375
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2475 7250 2625
Wire Wire Line
	7250 2275 7250 2000
Connection ~ 6750 2000
Wire Wire Line
	6750 2000 7250 2000
Connection ~ 6750 2625
Wire Wire Line
	6750 2625 7250 2625
Wire Wire Line
	6575 2000 6750 2000
Wire Wire Line
	6575 2625 6750 2625
Wire Wire Line
	7650 2275 7750 2275
Wire Wire Line
	7750 2275 7750 2000
Wire Wire Line
	7750 2000 8000 2000
Connection ~ 8000 2000
Wire Wire Line
	7650 2475 7750 2475
Wire Wire Line
	7750 2625 8000 2625
Wire Wire Line
	7750 2475 7750 2625
Wire Wire Line
	8000 2500 8000 2625
Connection ~ 8000 2625
Wire Wire Line
	8000 2000 8625 2000
Connection ~ 8625 2000
Wire Wire Line
	8625 2000 9750 2000
Wire Wire Line
	8000 2625 8575 2625
Connection ~ 8575 2625
Wire Wire Line
	8575 2625 9750 2625
Wire Wire Line
	8575 2625 8575 3850
Text HLabel 2750 6400 2    50   Output ~ 0
offset
$EndSCHEMATC
