EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title "Inverter Prototype Board"
Date "2019-09-18"
Rev "Draft"
Comp "Atelier Libre d'Électronique Pour l'Energie Auto-produite"
Comment1 "Gilles LONGUET"
Comment2 "Top level"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1750 2450 2    50   ~ 0
DC power input\n(12V - 400V)
Text Notes 10125 2375 0    50   ~ 0
230Vac
Text Label 2575 4825 0    50   ~ 0
leg1_H
Text Label 2575 4925 0    50   ~ 0
leg1_L
Text Label 2575 5025 0    50   ~ 0
leg2_H
Text Label 2575 5125 0    50   ~ 0
leg2_L
$Sheet
S 3300 2100 1900 1300
U 5D802728
F0 "H-brige" 50
F1 "H-brige.sch" 50
F2 "DC_high" I L 3300 2300 50 
F3 "DC_low" I L 3300 2400 50 
F4 "leg1_high_drv" I L 3300 2650 50 
F5 "leg1_low_drv" I L 3300 2750 50 
F6 "hached_low" O R 5200 2400 50 
F7 "leg2_high_drv" I L 3300 3000 50 
F8 "leg2_low_drv" I L 3300 3100 50 
F9 "temp" O R 5200 3250 50 
F10 "hached_high" O R 5200 2300 50 
$EndSheet
$Sheet
S 6500 2100 1900 1300
U 5D802989
F0 "Low pass filter and feedback" 50
F1 "Low_pass_filter_feedbacks.sch" 50
F2 "Vout_high" O R 8400 2300 50 
F3 "Vout_low" O R 8400 2400 50 
F4 "hached_high" I L 6500 2300 50 
F5 "hached_low" I L 6500 2400 50 
F6 "u_feedback" O R 8400 2925 50 
F7 "i_feedback" O R 8400 3050 50 
F8 "U_ref" O R 8400 2675 50 
F9 "offset" O R 8400 2800 50 
$EndSheet
$Sheet
S 6500 4100 1900 1300
U 5D802F85
F0 "Isolated power supply" 50
F1 "Isolated_power_supply.sch" 50
F2 "offset" I L 6500 4500 50 
F3 "i_iso" O R 8400 4500 50 
$EndSheet
Wire Wire Line
	2200 4600 2425 4600
Wire Wire Line
	2425 4600 2425 4825
Wire Wire Line
	2425 4825 3300 4825
Wire Wire Line
	2375 4925 2375 4800
Wire Wire Line
	2375 4800 2200 4800
Wire Wire Line
	2375 4925 3300 4925
Wire Wire Line
	2375 5025 2375 5000
Wire Wire Line
	2375 5000 2200 5000
Wire Wire Line
	2375 5025 3300 5025
Wire Wire Line
	2375 5125 2375 5200
Wire Wire Line
	2375 5200 2200 5200
Wire Wire Line
	2375 5125 3300 5125
Wire Wire Line
	2200 4700 2250 4700
Wire Wire Line
	2200 4900 2250 4900
Wire Wire Line
	2250 4900 2250 5100
Wire Wire Line
	2200 5100 2250 5100
Connection ~ 2250 5100
$Comp
L power:GND #PWR01
U 1 1 5D84A2BE
P 2250 5400
F 0 "#PWR01" H 2250 5150 50  0001 C CNN
F 1 "GND" H 2255 5227 50  0000 C CNN
F 2 "" H 2250 5400 50  0001 C CNN
F 3 "" H 2250 5400 50  0001 C CNN
	1    2250 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6350 2250 6350
$Comp
L power:GND #PWR03
U 1 1 5D85137A
P 2200 6450
F 0 "#PWR03" H 2200 6200 50  0001 C CNN
F 1 "GND" H 2205 6277 50  0000 C CNN
F 2 "" H 2200 6450 50  0001 C CNN
F 3 "" H 2200 6450 50  0001 C CNN
	1    2200 6450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2250 2300 2325 2300
Wire Wire Line
	3300 2400 2325 2400
Text Label 2775 2300 0    50   ~ 0
DC_high
Text Label 2775 2400 0    50   ~ 0
DC_low
Wire Wire Line
	9600 2300 9500 2300
Wire Wire Line
	8400 2400 9500 2400
Text Label 8700 2300 0    50   ~ 0
AC_high
Text Label 8700 2400 0    50   ~ 0
AC_low
Wire Wire Line
	5200 2300 6500 2300
Wire Wire Line
	6500 2400 5200 2400
$Sheet
S 3300 4100 1900 1300
U 5D824D19
F0 "Isolation and drivers" 50
F1 "Isolation_and_drivers.sch" 50
F2 "leg1_high" I L 3300 4825 50 
F3 "leg1_low" I L 3300 4925 50 
F4 "leg1_high_drv" O R 5200 4500 50 
F5 "leg1_low_drv" O R 5200 4700 50 
F6 "leg2_high" I L 3300 5025 50 
F7 "leg2_low" I L 3300 5125 50 
F8 "leg2_high_drv" O R 5200 5000 50 
F9 "leg2_low_drv" O R 5200 5200 50 
F10 "gates_disable" I L 3300 5300 50 
F11 "leg1_vs_drv" I R 5200 4600 50 
F12 "leg2_vs_drv" I R 5200 5100 50 
$EndSheet
Text Label 5750 4500 2    50   ~ 0
leg1_high_drv
Wire Wire Line
	5200 4500 5750 4500
Wire Wire Line
	5200 4700 5750 4700
Wire Wire Line
	5200 5000 5750 5000
Wire Wire Line
	5200 5200 5750 5200
Text Label 5750 4700 2    50   ~ 0
leg1_low_drv
Text Label 5750 5000 2    50   ~ 0
leg2_high_drv
Text Label 5750 5200 2    50   ~ 0
leg2_low_drv
Wire Wire Line
	2250 5100 2250 5300
Wire Wire Line
	2900 5300 3300 5300
Wire Wire Line
	5200 4600 5750 4600
Wire Wire Line
	5200 5100 5750 5100
Text Label 5750 5100 2    50   ~ 0
hached_low
Text Label 2750 2650 0    50   ~ 0
leg1_high_drv
Wire Wire Line
	3300 2650 2750 2650
Wire Wire Line
	3300 2750 2750 2750
Wire Wire Line
	3300 3000 2750 3000
Wire Wire Line
	3300 3100 2750 3100
Text Label 2750 2750 0    50   ~ 0
leg1_low_drv
Text Label 2750 3000 0    50   ~ 0
leg2_high_drv
Text Label 2750 3100 0    50   ~ 0
leg2_low_drv
Wire Wire Line
	8400 2925 8750 2925
Text Label 8750 2925 0    50   ~ 0
u_feedback
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 5DD9CC62
P 2050 3750
F 0 "J4" H 2050 3875 50  0000 C CNN
F 1 "24V" H 2050 3525 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_2-G_1x02_P5.00mm_Horizontal" H 2050 3750 50  0001 C CNN
F 3 "~" H 2050 3750 50  0001 C CNN
	1    2050 3750
	-1   0    0    -1  
$EndComp
$Comp
L power:+24V #PWR04
U 1 1 5DD9E39D
P 2375 3700
F 0 "#PWR04" H 2375 3550 50  0001 C CNN
F 1 "+24V" H 2390 3873 50  0000 C CNN
F 2 "" H 2375 3700 50  0001 C CNN
F 3 "" H 2375 3700 50  0001 C CNN
	1    2375 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2375 3700 2375 3750
Wire Wire Line
	2375 3750 2250 3750
Wire Wire Line
	2250 3850 2375 3850
Wire Wire Line
	2375 3850 2375 3900
$Comp
L power:GND #PWR05
U 1 1 5DDA0AF6
P 2375 3900
F 0 "#PWR05" H 2375 3650 50  0001 C CNN
F 1 "GND" H 2380 3727 50  0000 C CNN
F 2 "" H 2375 3900 50  0001 C CNN
F 3 "" H 2375 3900 50  0001 C CNN
	1    2375 3900
	1    0    0    -1  
$EndComp
Text Notes 1750 3950 2    50   ~ 0
DC input in order to create\n15V_iso and 5V_iso
$Comp
L power:VCC #PWR02
U 1 1 5DDA4ECE
P 2250 6300
F 0 "#PWR02" H 2250 6150 50  0001 C CNN
F 1 "VCC" H 2267 6473 50  0000 C CNN
F 2 "" H 2250 6300 50  0001 C CNN
F 3 "" H 2250 6300 50  0001 C CNN
	1    2250 6300
	1    0    0    -1  
$EndComp
Text Label 8750 3050 0    50   ~ 0
i_feedback
Wire Wire Line
	8400 3050 8750 3050
Text Label 6300 4500 2    50   ~ 0
offset
Wire Wire Line
	6500 4500 6300 4500
Wire Wire Line
	8400 4500 8750 4500
Text Label 8750 4500 0    50   ~ 0
i_iso
$Comp
L Connector:RJ45 J1
U 1 1 5DEFBEE3
P 1800 5000
F 0 "J1" H 1857 5667 50  0000 C CNN
F 1 "RJ45" H 1857 5576 50  0000 C CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 1800 5025 50  0001 C CNN
F 3 "~" V 1800 5025 50  0001 C CNN
	1    1800 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5300 2250 5300
Connection ~ 2250 5300
Wire Wire Line
	2250 5300 2250 5400
Wire Wire Line
	2250 4900 2250 4700
Connection ~ 2250 4900
$Comp
L Connector:RJ45 J2
U 1 1 5DF037FB
P 1800 6750
F 0 "J2" H 1857 7417 50  0000 C CNN
F 1 "RJ45" H 1857 7326 50  0000 C CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 1800 6775 50  0001 C CNN
F 3 "~" V 1800 6775 50  0001 C CNN
	1    1800 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6300 2250 6350
Text Label 2900 5300 2    50   ~ 0
gates_disable
Wire Wire Line
	2400 7050 2200 7050
Text Label 2400 7050 0    50   ~ 0
gates_disable
NoConn ~ 2200 6650
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5DF221B5
P 2750 3700
F 0 "#FLG01" H 2750 3775 50  0001 C CNN
F 1 "PWR_FLAG" H 2750 3873 50  0000 C CNN
F 2 "" H 2750 3700 50  0001 C CNN
F 3 "~" H 2750 3700 50  0001 C CNN
	1    2750 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 3700 2750 3750
Wire Wire Line
	2750 3750 2375 3750
Connection ~ 2375 3750
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5DF23E1A
P 2750 3900
F 0 "#FLG02" H 2750 3975 50  0001 C CNN
F 1 "PWR_FLAG" H 2750 4073 50  0000 C CNN
F 2 "" H 2750 3900 50  0001 C CNN
F 3 "~" H 2750 3900 50  0001 C CNN
	1    2750 3900
	1    0    0    1   
$EndComp
Wire Wire Line
	2750 3900 2750 3850
Wire Wire Line
	2750 3850 2375 3850
Connection ~ 2375 3850
Wire Wire Line
	5200 6350 5250 6350
Text Label 5400 7050 0    50   ~ 0
u_feedback
Text Label 5400 6950 0    50   ~ 0
i_feedback
Wire Wire Line
	5200 7050 5400 7050
$Comp
L power:VCC #PWR06
U 1 1 5DF38EB2
P 5250 6300
F 0 "#PWR06" H 5250 6150 50  0001 C CNN
F 1 "VCC" H 5267 6473 50  0000 C CNN
F 2 "" H 5250 6300 50  0001 C CNN
F 3 "" H 5250 6300 50  0001 C CNN
	1    5250 6300
	1    0    0    -1  
$EndComp
Text Label 5400 6850 0    50   ~ 0
i_iso
$Comp
L Connector:RJ45 J5
U 1 1 5DF38EB9
P 4800 6750
F 0 "J5" H 4857 7417 50  0000 C CNN
F 1 "RJ45" H 4857 7326 50  0000 C CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 4800 6775 50  0001 C CNN
F 3 "~" V 4800 6775 50  0001 C CNN
	1    4800 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 6300 5250 6350
Wire Wire Line
	5200 6850 5400 6850
Wire Wire Line
	5200 6950 5400 6950
$Comp
L power:GND #PWR07
U 1 1 5DF49706
P 5200 6450
F 0 "#PWR07" H 5200 6200 50  0001 C CNN
F 1 "GND" H 5205 6277 50  0000 C CNN
F 2 "" H 5200 6450 50  0001 C CNN
F 3 "" H 5200 6450 50  0001 C CNN
	1    5200 6450
	0    -1   -1   0   
$EndComp
NoConn ~ 5200 6550
NoConn ~ 2200 6550
NoConn ~ 2200 6750
NoConn ~ 2200 6850
NoConn ~ 2200 6950
Text Notes 1275 4975 2    50   ~ 0
H-bridge PWM\nconnector
Text Notes 1275 6700 2    50   ~ 0
digital I/O signals\nconnector
Text Notes 4275 6700 2    50   ~ 0
analog signals\nconnector
Text Notes 6725 900  2    118  ~ 24
Inverter Prototype Board
Wire Notes Line
	4500 1000 6750 1000
Wire Wire Line
	8400 2675 8750 2675
Text Label 8750 2675 0    50   ~ 0
u_ref
Wire Wire Line
	5200 6750 5400 6750
Text Label 5400 6750 0    50   ~ 0
u_ref
Wire Wire Line
	2250 2200 2325 2200
Wire Wire Line
	2325 2200 2325 2300
Connection ~ 2325 2300
Wire Wire Line
	2325 2300 3300 2300
Wire Wire Line
	2325 2400 2325 2500
Wire Wire Line
	2325 2500 2250 2500
Connection ~ 2325 2400
Wire Wire Line
	2325 2400 2250 2400
$Comp
L Connector:Screw_Terminal_01x04 J6
U 1 1 5DF57611
P 9800 2400
F 0 "J6" H 9718 1975 50  0000 C CNN
F 1 "Vac" H 9718 2066 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_4-G-5,08_1x04_P5.08mm_Horizontal" H 9800 2400 50  0001 C CNN
F 3 "~" H 9800 2400 50  0001 C CNN
	1    9800 2400
	1    0    0    1   
$EndComp
Wire Wire Line
	9600 2200 9500 2200
Wire Wire Line
	9500 2200 9500 2300
Connection ~ 9500 2300
Wire Wire Line
	9500 2300 8400 2300
Wire Wire Line
	9500 2400 9500 2500
Wire Wire Line
	9500 2500 9600 2500
Connection ~ 9500 2400
Wire Wire Line
	9500 2400 9600 2400
$Comp
L Connector:Screw_Terminal_01x04 J3
U 1 1 5DF5C3A8
P 2050 2300
F 0 "J3" H 1968 2617 50  0000 C CNN
F 1 "DCbus" H 1968 2526 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_4-G-5,08_1x04_P5.08mm_Horizontal" H 2050 2300 50  0001 C CNN
F 3 "~" H 2050 2300 50  0001 C CNN
	1    2050 2300
	-1   0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5DF6411C
P 10000 6000
F 0 "H3" H 10100 6046 50  0000 L CNN
F 1 "MountingHole" H 10100 5955 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad" H 10000 6000 50  0001 C CNN
F 3 "~" H 10000 6000 50  0001 C CNN
	1    10000 6000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5DF64451
P 10000 5750
F 0 "H2" H 10100 5796 50  0000 L CNN
F 1 "MountingHole" H 10100 5705 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad" H 10000 5750 50  0001 C CNN
F 3 "~" H 10000 5750 50  0001 C CNN
	1    10000 5750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5DF64642
P 10000 6250
F 0 "H4" H 10100 6296 50  0000 L CNN
F 1 "MountingHole" H 10100 6205 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad" H 10000 6250 50  0001 C CNN
F 3 "~" H 10000 6250 50  0001 C CNN
	1    10000 6250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5DF648C8
P 10000 5500
F 0 "H1" H 10100 5546 50  0000 L CNN
F 1 "MountingHole" H 10100 5455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad" H 10000 5500 50  0001 C CNN
F 3 "~" H 10000 5500 50  0001 C CNN
	1    10000 5500
	1    0    0    -1  
$EndComp
Text Label 5750 4600 2    50   ~ 0
hached_high
Wire Wire Line
	5200 6650 5400 6650
Text Label 5400 6650 0    50   ~ 0
temp
Wire Wire Line
	5200 3250 5400 3250
Text Label 5400 3250 0    50   ~ 0
temp
Wire Wire Line
	8400 2800 8750 2800
Text Label 8750 2800 0    50   ~ 0
offset
$EndSCHEMATC
